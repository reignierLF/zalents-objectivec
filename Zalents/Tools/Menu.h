//
//  Menu.h
//  Zalents
//
//  Created by LF-Mac-Air on 23/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Settings : NSObject

@property (nonatomic, readwrite) float height;
@property (nonatomic, readwrite) UIFont *font;
@property (nonatomic, readwrite) UIColor *textColor;
@property (nonatomic, readwrite) UIColor *menuBackgroundColor;
@property (nonatomic, readwrite) UIColor *subMenuBackgroundColor;
@property (nonatomic, readwrite) UIColor *linebreakColor;

@end

@interface Menu : NSObject

@property (nonatomic, readonly) NSArray *list;
@property (nonatomic, readonly) Settings *settings;
@property (nonatomic, readonly) NSArray *viewControllers;

@end