//
//  Menu.m
//  Zalents
//
//  Created by LF-Mac-Air on 23/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "Menu.h"

#import "HomeViewController.h"

@implementation Settings

-(id)init{
    
    self = [super init];
    
    if(self){
        
        /*
         * Default settings
         *
         * This is the default settings, incase we didnt put anything to set
         * for our menu controller
         */
        
        
        /*
         * For height of menu button for each row
         */
        
        [self setHeight:40];
        
        /*
         * For font of menu button for each row
         */
        
        [self setFont:[UIFont systemFontOfSize:13]];
        
        /*
         * For text color of menu button for each row
         */
        
        [self setTextColor:[UIColor grayColor]];
        
        /*
         * For background color of menu button for each row
         */
        
        [self setMenuBackgroundColor:[UIColor whiteColor]];
        
        /*
         * For background color of sub-menu button for each row
         */
        
        [self setSubMenuBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.1]];
        
        /*
         * For linebreak of of each menu and sub-menu button for each row
         */
        
        [self setLinebreakColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.2]];
    }
    
    return self;
}

/*
 * We are saving the settings as persistent data
 */

-(void)setHeight:(float)height{
    [[NSUserDefaults standardUserDefaults] setFloat:height forKey:@"setting_height"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //NSLog(@"set height");
}

-(void)setFont:(UIFont *)font{
    NSData *fontData = [NSKeyedArchiver archivedDataWithRootObject:font];
    
    [[NSUserDefaults standardUserDefaults] setObject:fontData forKey:@"setting_font"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //NSLog(@"set font");
}

-(void)setTextColor:(UIColor *)textColor{
    NSData *textColorData = [NSKeyedArchiver archivedDataWithRootObject:textColor];
    
    [[NSUserDefaults standardUserDefaults] setObject:textColorData forKey:@"setting_textColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //NSLog(@"set text color");
}

-(void)setMenuBackgroundColor:(UIColor *)menuBackgroundColor{
    NSData *menuBackgroundColorData = [NSKeyedArchiver archivedDataWithRootObject:menuBackgroundColor];
    
    [[NSUserDefaults standardUserDefaults] setObject:menuBackgroundColorData forKey:@"setting_menuBackgroundColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //NSLog(@"set menu bg color");
}

-(void)setSubMenuBackgroundColor:(UIColor *)subMenuBackgroundColor{
    NSData *subMenuBackgroundColorData = [NSKeyedArchiver archivedDataWithRootObject:subMenuBackgroundColor];
    
    [[NSUserDefaults standardUserDefaults] setObject:subMenuBackgroundColorData forKey:@"setting_subMenuBackgroundColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //NSLog(@"set sub menu bg color");
}

-(void)setLinebreakColor:(UIColor *)linebreakColor{
    NSData *linebreakColorData = [NSKeyedArchiver archivedDataWithRootObject:linebreakColor];
    
    [[NSUserDefaults standardUserDefaults] setObject:linebreakColorData forKey:@"setting_linebreakColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //NSLog(@"set linebreak color");
}

@end

@implementation Menu

-(instancetype)init{
    self = [super init];
    
    if(self){
        
        _settings = [[Settings alloc] init];
        
        /*
         * Menu display
         */
        
        HomeViewController *home = [[HomeViewController alloc] init];
        home.title = @"Home";
        
        _viewControllers = @[
                           @[ home ],
                           @[ @"Discover" ],
                           @[ @"My Classes" ],
                           @[ @"Notifications" ],
                           @[ @"Account" ],
                           @[ @"Search" ],
                           ];
        
        _list =   @[
                  @[ home.title ],
                  @[ @"Discover" ],
                  @[ @"My Classes" ],
                  @[ @"Notifications" ],
                  @[ @"Account" ],
                  @[ @"Search" ],
                  ];
    }
    
    return self;
}

@end
