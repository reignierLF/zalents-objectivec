//
//  MenuController.h
//  Zalents
//
//  Created by LF-Mac-Air on 23/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Menu.h"

/*
 * Delegate / Protocol
 */

@class MenuController;
@protocol MenuControllerDelegate
- (void)didSelectMenu:(MenuController*)menuController row:(NSInteger)row section:(NSInteger)section;
@end

typedef void(^Block)(void);

@interface MenuController : UIView{
    Block block;
}

@property (nonatomic, readonly) NSArray *matrixToList;
@property (nonatomic, readonly) UIViewController *currentViewController;

@property (nonatomic, strong) Menu *menu;

@property (nonatomic, weak) id <MenuControllerDelegate> delegate;

-(void)initMenuBarItem;

-(void)toogleMenuView;

/*
 *
 */

@property (nonatomic, copy) Block menuViewDisappear;

@end
