//
//  WelcomeMessage.h
//  Zalents
//
//  Created by LF-Mac-Air on 23/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WelcomeMessage : NSObject

@property (nonatomic, readonly) NSArray *titles;
@property (nonatomic, readonly) NSArray *messages;
@property (nonatomic, readonly) NSArray *images;

@property (nonatomic) NSInteger count;

@end
