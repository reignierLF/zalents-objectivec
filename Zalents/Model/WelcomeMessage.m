//
//  WelcomeMessage.m
//  Zalents
//
//  Created by LF-Mac-Air on 23/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "WelcomeMessage.h"

@implementation WelcomeMessage

-(id)init{
    self = [super init];
    
    if(self){
        _images= [[NSArray alloc] initWithObjects:
                  [UIImage imageNamed:@"welcome_1.jpg"],
                  [UIImage imageNamed:@"welcome_2.jpg"],
                  [UIImage imageNamed:@"welcome_3.jpg"],
                  [UIImage imageNamed:@"welcome_4.jpg"],
                  nil];
        
        _titles = [[NSArray alloc] initWithObjects:
                   @"EXCLUSIVE EMPLOYEE ACCESS \nTO WORLD CLASS PROGRAMMES",
                   @"BEGIN YOUR JOURNEY",
                   @"BOOK IT IN A SNAP",
                   @"WIN EXCLUSIVE PRIZES",
                   nil];

        
        _messages = [[NSArray alloc] initWithObjects:
                     @"",
                     @"Unleash Your Creativity. \nDiscover True Innovation. \nConquer Your Summits.",
                     @"Discover New Programs, \nRegister Your Attendance - All Paid For \nWith Your Exclusive SUMMIT Dollars.",
                     @"Increase Your Chances - With Each Quiz You Answer, \nEach Friend You Invite To Join! \nLearn & Win.",
                     nil];
        

        
        _count = _images.count;
    }
    
    return self;
}

@end
