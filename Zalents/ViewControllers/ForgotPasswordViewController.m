//
//  ForgotPasswordViewController.m
//  Zalents
//
//  Created by LF-Mac-Air on 23/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "WelcomeMessage.h"

@interface ForgotPasswordViewController ()

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) WelcomeMessage *wm;

@property (nonatomic, strong) UIImageView *welcomeImageView;

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
    [self initLoginLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _wm = [[WelcomeMessage alloc] init];
}

-(void)initBackgroundImage{
    _welcomeImageView = [[UIImageView alloc] initWithImage:[_wm.images objectAtIndex:0]];
    _welcomeImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    _welcomeImageView.contentMode = UIViewContentModeScaleAspectFill;
    _welcomeImageView.clipsToBounds = YES;
    [self.view addSubview:_welcomeImageView];
    
    UIView *dimView = [[UIView alloc] initWithFrame:_welcomeImageView.bounds];
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    [self.view addSubview:dimView];
}

-(void)initLoginLayout{
    UILabel *activationLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10 + (_screenHeight / 8), _screenWidth - 40, 40)];
    //loginLabel.backgroundColor = [UIColor redColor];
    activationLabel.text = @"We will send password reset instruction to your email address";
    activationLabel.textColor = [UIColor whiteColor];
    activationLabel.numberOfLines = 0;
    activationLabel.lineBreakMode = NSLineBreakByWordWrapping;
    activationLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:activationLabel];
    
    UIView *inputContainerView = [[UIView alloc] initWithFrame:CGRectMake(activationLabel.frame.origin.x, activationLabel.frame.origin.y + activationLabel.frame.size.height + 10, activationLabel.frame.size.width, activationLabel.frame.size.height)];
    inputContainerView.backgroundColor = [UIColor whiteColor];
    inputContainerView.layer.cornerRadius = 2;
    [self.view addSubview:inputContainerView];
    
    UITextField *activationTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, 0, activationLabel.frame.size.width - 10, activationLabel.frame.size.height)];
    //emailTextField.backgroundColor = [UIColor greenColor];
    activationTextField.text = @"Email Address";
    activationTextField.textColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
    activationTextField.font = [UIFont systemFontOfSize:15];
    [inputContainerView addSubview:activationTextField];
    
    UIButton *activateButton = [UIButton buttonWithType:UIButtonTypeSystem];
    activateButton.frame = CGRectMake(inputContainerView.frame.origin.x, inputContainerView.frame.origin.y + inputContainerView.frame.size.height + 10, inputContainerView.frame.size.width, activationTextField.frame.size.height);
    activateButton.layer.cornerRadius = 3;
    activateButton.layer.borderWidth = 1;
    activateButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [activateButton setTitle:@"Reset my password" forState:UIControlStateNormal];
    [activateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:activateButton];
}

@end
