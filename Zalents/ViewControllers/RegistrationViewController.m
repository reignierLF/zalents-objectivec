//
//  RegistrationViewController.m
//  Zalents
//
//  Created by LF-Mac-Air on 6/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "RegistrationViewController.h"
#import "WelcomeMessage.h"

@interface RegistrationViewController ()

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) WelcomeMessage *wm;

@property (nonatomic, strong) UIImageView *welcomeImageView;

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
    [self initLoginLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _wm = [[WelcomeMessage alloc] init];
}

-(void)initBackgroundImage{
    _welcomeImageView = [[UIImageView alloc] initWithImage:[_wm.images objectAtIndex:0]];
    _welcomeImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    _welcomeImageView.contentMode = UIViewContentModeScaleAspectFill;
    _welcomeImageView.clipsToBounds = YES;
    [self.view addSubview:_welcomeImageView];
    
    UIView *dimView = [[UIView alloc] initWithFrame:_welcomeImageView.bounds];
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    [self.view addSubview:dimView];
}

-(void)initLoginLayout{
    UILabel *registerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 70, _screenWidth - 40, 100)];
    registerLabel.backgroundColor = [UIColor redColor];
    //registerLabel.text = @"Welcome to Zalents";
    registerLabel.textColor = [UIColor whiteColor];
    registerLabel.font = [UIFont boldSystemFontOfSize:30];
    registerLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:registerLabel];
    
    UIView *inputContainerView = [[UIView alloc] initWithFrame:CGRectMake(20, registerLabel.frame.origin.y + registerLabel.frame.size.height + 50, registerLabel.frame.size.width, 40)];
    inputContainerView.backgroundColor = [UIColor whiteColor];
    inputContainerView.layer.cornerRadius = 6;
    [self.view addSubview:inputContainerView];
    
    UITextField *emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, 0, inputContainerView.frame.size.width - 10, inputContainerView.frame.size.height)];
    //emailTextField.backgroundColor = [UIColor greenColor];
    emailTextField.text = @"Personal access code";
    emailTextField.textAlignment = NSTextAlignmentCenter;
    emailTextField.textColor = [UIColor lightGrayColor];
    emailTextField.font = [UIFont systemFontOfSize:15];
    [inputContainerView addSubview:emailTextField];
    
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeSystem];
    registerButton.frame = CGRectMake(inputContainerView.frame.origin.x, inputContainerView.frame.origin.y + inputContainerView.frame.size.height + 10, inputContainerView.frame.size.width, emailTextField.frame.size.height);
    registerButton.layer.cornerRadius = 6;
    registerButton.layer.borderWidth = 0;
    registerButton.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    registerButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [registerButton setTitle:@"Next" forState:UIControlStateNormal];
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerButton setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:registerButton];
    
    [registerButton addTarget:self action:@selector(loadActivationPage) forControlEvents:UIControlEventTouchUpInside];
}

/*
 * Event
 */

-(void)loadActivationPage{
    
    //ActivationViewController *act = [[ActivationViewController alloc] init];
    //[self.navigationController pushViewController:act animated:YES];
}
@end
