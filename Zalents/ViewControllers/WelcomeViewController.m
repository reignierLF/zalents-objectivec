//
//  WelcomeViewController.m
//  Zalents
//
//  Created by LF-Mac-Air on 5/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "WelcomeViewController.h"
#import "RegistrationViewController.h"
#import "LoginViewController.h"

#import "WelcomeMessage.h"

//Dev testing purposes
#import "MenuController.h"
#import "Menu.h"

@interface WelcomeViewController () <UIScrollViewDelegate, UIPageViewControllerDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) WelcomeMessage *wm;

@property (nonatomic, strong) UIImageView *welcomeImageView;
@property (nonatomic, strong) UIPageControl *pageControl;

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
    [self initPagination];
    [self initButtons];
}

-(void)initialize{
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _wm = [[WelcomeMessage alloc] init];
}

-(void)initBackgroundImage{
    _welcomeImageView = [[UIImageView alloc] initWithImage:[_wm.images objectAtIndex:0]];
    _welcomeImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    _welcomeImageView.contentMode = UIViewContentModeScaleAspectFill;
    _welcomeImageView.clipsToBounds = YES;
    [self.view addSubview:_welcomeImageView];
    
    UIView *dimView = [[UIView alloc] initWithFrame:_welcomeImageView.bounds];
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    [self.view addSubview:dimView];
}

-(void)initPagination{
    UIScrollView *pagerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    pagerScrollView.backgroundColor = [UIColor clearColor];
    pagerScrollView.delegate = self;
    pagerScrollView.bounces = NO;
    pagerScrollView.pagingEnabled = YES;
    pagerScrollView.contentSize = CGSizeMake(pagerScrollView.frame.size.width * _wm.images.count, pagerScrollView.frame.size.height- 20);
    [self.view addSubview:pagerScrollView];
    
    for (int i = 0; i < _wm.count; i++) {
        UIView *welcomeView = [[UIView alloc] initWithFrame:CGRectMake(_screenWidth * i, 0, _screenWidth, _screenHeight)];
        //welcomeView.backgroundColor = [UIColor orangeColor];
        [pagerScrollView addSubview:welcomeView];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _screenHeight / 2, _screenWidth - 40, _screenHeight / 6)];
        //titleLabel.backgroundColor = [UIColor redColor];
        titleLabel.text = [_wm.titles objectAtIndex:i];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [welcomeView addSubview:titleLabel];
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, titleLabel.frame.origin.y + titleLabel.frame.size.height, _screenWidth - 40, _screenHeight / 6)];
        //messageLabel.backgroundColor = [UIColor greenColor];
        messageLabel.text = [_wm.messages objectAtIndex:i];
        messageLabel.textColor = [UIColor whiteColor];
        messageLabel.numberOfLines = 0;
        messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [welcomeView addSubview:messageLabel];
    }
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, _screenHeight - (_screenHeight / 6), _screenWidth, 30)];
    //_pageControl.backgroundColor = [UIColor blackColor];
    _pageControl.numberOfPages = _wm.count;
    _pageControl.autoresizingMask = UIViewAutoresizingNone;
    [self.view addSubview:_pageControl];
}

-(void)initButtons{
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeSystem];
    registerButton.frame = CGRectMake(20, (_screenHeight - (_screenHeight / 6)) + _pageControl.frame.size.height + 10, (_screenWidth / 2) - 30, ((_screenHeight / 6) - _pageControl.frame.size.height) - 30);
    //registerButton.backgroundColor = [UIColor blueColor];
    registerButton.layer.cornerRadius = 6;
    registerButton.layer.borderWidth = 2;
    registerButton.layer.borderColor = [UIColor blueColor].CGColor;
    registerButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [registerButton setTitle:@"SIGN UP" forState:UIControlStateNormal];
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerButton setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:registerButton];
    
    [registerButton addTarget:self action:@selector(loadRegistrationPage) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    loginButton.frame = CGRectMake((_screenWidth - 20) - registerButton.frame.size.width, registerButton.frame.origin.y, registerButton.frame.size.width, registerButton.frame.size.height);
    //loginButton.backgroundColor = [UIColor blueColor];
    loginButton.layer.cornerRadius = 6;
    loginButton.layer.borderWidth = 2;
    loginButton.layer.borderColor = [UIColor whiteColor].CGColor;
    loginButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [loginButton setTitle:@"LOG IN" forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginButton setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:loginButton];
    
    [loginButton addTarget:self action:@selector(loadLoginPage) forControlEvents:UIControlEventTouchUpInside];
}

/*
 * Events
 */

-(void)loadRegistrationPage{
    RegistrationViewController *reg = [[RegistrationViewController alloc] init];
    [self.navigationController pushViewController:reg animated:YES];
}

-(void)loadLoginPage{
    LoginViewController *log = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:log animated:YES];
}

/*
 * Delegates
 */

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    _pageControl.currentPage = fabs(((scrollView.contentSize.width - scrollView.contentOffset.x) / scrollView.frame.size.width) - _wm.count);
    
    UIImage * nextImage = [_wm.images objectAtIndex:_pageControl.currentPage];
    
    [UIView transitionWithView:_welcomeImageView
                      duration:0.35f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _welcomeImageView.image = nextImage;
                    }completion:nil];
}
@end
