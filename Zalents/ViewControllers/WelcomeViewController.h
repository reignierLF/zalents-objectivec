//
//  WelcomeViewController.h
//  Zalents
//
//  Created by LF-Mac-Air on 5/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController <UIScrollViewDelegate, UIPageViewControllerDelegate>

@end
