//
//  LoginViewController.m
//  Zalents
//
//  Created by LF-Mac-Air on 6/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "LoginViewController.h"
#import "WelcomeMessage.h"

#import "HomeViewController.h"
#import "ForgotPasswordViewController.h"

@interface LoginViewController ()

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) WelcomeMessage *wm;

@property (nonatomic, strong) UIImageView *welcomeImageView;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
    [self initLoginLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _wm = [[WelcomeMessage alloc] init];
}

-(void)initBackgroundImage{
    _welcomeImageView = [[UIImageView alloc] initWithImage:[_wm.images objectAtIndex:0]];
    _welcomeImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    _welcomeImageView.contentMode = UIViewContentModeScaleAspectFill;
    _welcomeImageView.clipsToBounds = YES;
    [self.view addSubview:_welcomeImageView];
    
    UIView *dimView = [[UIView alloc] initWithFrame:_welcomeImageView.bounds];
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    [self.view addSubview:dimView];
}

-(void)initLoginLayout{
    UIImageView *zalentsTitleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insert here"]];
    zalentsTitleImageView.frame = CGRectMake((_screenWidth / 2) - ((_screenWidth - 60) / 2), 150, _screenWidth - 60, 100);
    zalentsTitleImageView.backgroundColor = [UIColor orangeColor];
    zalentsTitleImageView.contentMode = UIViewContentModeScaleAspectFill;
    zalentsTitleImageView.clipsToBounds = YES;
    [self.view addSubview:zalentsTitleImageView];
    
    UITextField *emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(30, zalentsTitleImageView.frame.origin.y + zalentsTitleImageView.frame.size.height + 20, zalentsTitleImageView.frame.size.width, 40)];
    emailTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    emailTextField.text = @"Email Address";
    emailTextField.textColor = [UIColor whiteColor];
    emailTextField.font = [UIFont systemFontOfSize:15];
    emailTextField.textAlignment = NSTextAlignmentCenter;
    emailTextField.layer.cornerRadius = 6;
    [self.view addSubview:emailTextField];
    
    UITextField *passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(emailTextField.frame.origin.x, emailTextField.frame.origin.y + emailTextField.frame.size.height + 10, emailTextField.frame.size.width, emailTextField.frame.size.height)];
    passwordTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    passwordTextField.text = @"Password";
    passwordTextField.textColor = [UIColor whiteColor];
    passwordTextField.font = [UIFont systemFontOfSize:15];
    passwordTextField.textAlignment = NSTextAlignmentCenter;
    passwordTextField.layer.cornerRadius = 6;
    [self.view addSubview:passwordTextField];
    
    UIButton *forgotPasswordButton = [UIButton buttonWithType:UIButtonTypeSystem];
    forgotPasswordButton.frame = CGRectMake(passwordTextField.frame.origin.x, passwordTextField.frame.origin.y + passwordTextField.frame.size.height + 2, passwordTextField.frame.size.width, 10);
    forgotPasswordButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [forgotPasswordButton setTitle:@"Forgot your password?" forState:UIControlStateNormal];
    [forgotPasswordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    float newForgotPasswordButtonWidth = forgotPasswordButton.titleLabel.intrinsicContentSize.width;
    
    forgotPasswordButton.frame = CGRectMake(_screenWidth - (newForgotPasswordButtonWidth + 30), forgotPasswordButton.frame.origin.y, newForgotPasswordButtonWidth, forgotPasswordButton.frame.size.height);
    [self.view addSubview:forgotPasswordButton];
    
    [forgotPasswordButton addTarget:self action:@selector(loadForgotPasswordPage) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    loginButton.frame = CGRectMake((_screenWidth / 2) - 50, passwordTextField.frame.origin.y + passwordTextField.frame.size.height + 30, 100, 30);
    loginButton.layer.cornerRadius = 6;
    loginButton.layer.borderWidth = 2;
    loginButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [loginButton setTitle:@"Log in" forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:loginButton];
    
    [loginButton addTarget:self action:@selector(loadHomePage) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake((_screenWidth / 2) - ((passwordTextField.frame.size.width - 20) / 2), loginButton.frame.origin.y + loginButton.frame.size.height + 20, passwordTextField.frame.size.width - 20, 1)];
    seperator.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:seperator];
    
    UILabel *signUpLabel = [[UILabel alloc] initWithFrame:CGRectMake(passwordTextField.frame.origin.x, seperator.frame.origin.y + seperator.frame.size.height + 5, 100, 10)];
    signUpLabel.text = @"Don't have an account?";
    signUpLabel.textColor = [UIColor whiteColor];
    signUpLabel.font = [UIFont systemFontOfSize:14];
    
    float newSignUpLabelWidth = signUpLabel.intrinsicContentSize.width;
    
    UIButton *signUpButton = [UIButton buttonWithType:UIButtonTypeSystem];
    signUpButton.frame = CGRectMake(passwordTextField.frame.origin.x, passwordTextField.frame.origin.y + passwordTextField.frame.size.height + 2, passwordTextField.frame.size.width, 10);
    signUpButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [signUpButton setTitle:@"Sign Up?" forState:UIControlStateNormal];
    [signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    float newSignUpButtonWidth = signUpButton.titleLabel.intrinsicContentSize.width;
    
    signUpLabel.frame = CGRectMake((_screenWidth / 2) - ((newSignUpLabelWidth + newSignUpButtonWidth) / 2), signUpLabel.frame.origin.y, newSignUpLabelWidth, signUpLabel.frame.size.height);
    [self.view addSubview:signUpLabel];
    
    signUpButton.frame = CGRectMake(signUpLabel.frame.origin.x + signUpLabel.frame.size.width + 2, signUpLabel.frame.origin.y, newSignUpButtonWidth, signUpLabel.frame.size.height);
    [self.view addSubview:signUpButton];
    
    [signUpButton addTarget:self action:@selector(loadSignUpPage) forControlEvents:UIControlEventTouchUpInside];
}

-(void)loadHomePage{
    HomeViewController *hom = [[HomeViewController alloc] init];
    [self.navigationController pushViewController:hom animated:YES];
}

-(void)loadForgotPasswordPage{
    ForgotPasswordViewController *forg = [[ForgotPasswordViewController alloc] init];
    [self.navigationController pushViewController:forg animated:YES];
}

-(void)loadSignUpPage{
    NSLog(@"load sign up");
}
@end
