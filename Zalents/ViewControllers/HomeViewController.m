//
//  HomeViewController.m
//  Zalents
//
//  Created by LF-Mac-Air on 5/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "HomeViewController.h"
#import "MenuController.h"

@interface HomeViewController () <MenuControllerDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) MenuController *mc;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    //[self initHomeLayout];
    
    [self initMenuController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIColor *zalentsColorTheme = [UIColor colorWithRed:5/255.f green:11/255.f blue:103/255.f alpha:1.0];
    
    [self.navigationController.navigationBar setBarTintColor:zalentsColorTheme];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height - (self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height);
}

-(void)initHomeLayout{
    UIImageView *uobSummitImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"uobSummitHome"]];
    uobSummitImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight / 2);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    uobSummitImageView.contentMode = UIViewContentModeScaleAspectFill;
    uobSummitImageView.clipsToBounds = YES;
    [self.view addSubview:uobSummitImageView];
    
    UIView *uobSummitIntroView = [[UIView alloc] initWithFrame:CGRectMake(0, uobSummitImageView.frame.size.height - 140, uobSummitImageView.frame.size.width, 140)];
    uobSummitIntroView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    [uobSummitImageView addSubview:uobSummitIntroView];
    
    UILabel *uobSummitHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, uobSummitImageView.frame.size.width - 20, 40)];
    //uobSummitHeaderLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    uobSummitHeaderLabel.text = @"UOB Summit Workshop";
    uobSummitHeaderLabel.textColor = [UIColor whiteColor];
    uobSummitHeaderLabel.numberOfLines = 0;
    uobSummitHeaderLabel.lineBreakMode = NSLineBreakByWordWrapping;
    uobSummitHeaderLabel.font = [UIFont boldSystemFontOfSize:24];
    [uobSummitIntroView addSubview:uobSummitHeaderLabel];
    
    UILabel *uobSummitTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, uobSummitHeaderLabel.frame.origin.y + uobSummitHeaderLabel.frame.size.height + 10, uobSummitIntroView.frame.size.width - (uobSummitIntroView.frame.size.width / 3), 20)];
    //uobSummitTitleLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    uobSummitTitleLabel.text = @"What is UOB Summit?";
    uobSummitTitleLabel.textColor = [UIColor whiteColor];
    uobSummitTitleLabel.numberOfLines = 0;
    uobSummitTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    uobSummitTitleLabel.font = [UIFont boldSystemFontOfSize:16];
    [uobSummitIntroView addSubview:uobSummitTitleLabel];
    
    UILabel *uobSummitDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(uobSummitTitleLabel.frame.origin.x, uobSummitTitleLabel.frame.origin.y + uobSummitTitleLabel.frame.size.height, uobSummitIntroView.frame.size.width  - 20, 60)];
    //uobSummitDescriptionLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    uobSummitDescriptionLabel.text = @"This is a holistic development initiative for our full-time permanent employees (up to Manager level) utilizing the funding received from the government under.... read more";
    uobSummitDescriptionLabel.textColor = [UIColor whiteColor];
    uobSummitDescriptionLabel.font = [UIFont italicSystemFontOfSize:12];
    uobSummitDescriptionLabel.numberOfLines = 0;
    uobSummitDescriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [uobSummitIntroView addSubview:uobSummitDescriptionLabel];
    
    UIButton *uobSummitButton = [UIButton buttonWithType:UIButtonTypeSystem];
    uobSummitButton.frame = CGRectMake(0, 0, _screenWidth, _screenHeight / 2);
    //uobSummitButton.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:uobSummitButton];
    
    [uobSummitButton addTarget:self action:@selector(loadUobSummitPage) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *skillsFutureImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"skillsFutureHome"]];
    skillsFutureImageView.frame = CGRectMake(0, uobSummitImageView.frame.origin.y + uobSummitImageView.frame.size.height, _screenWidth, _screenHeight / 2);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    skillsFutureImageView.contentMode = UIViewContentModeScaleAspectFill;
    skillsFutureImageView.clipsToBounds = YES;
    [self.view addSubview:skillsFutureImageView];
    
    UIView *skillsFutureIntroView = [[UIView alloc] initWithFrame:CGRectMake(0, skillsFutureImageView.frame.size.height - 140, skillsFutureImageView.frame.size.width, 140)];
    skillsFutureIntroView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    [skillsFutureImageView addSubview:skillsFutureIntroView];
    
    UILabel *skillsFutureHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, skillsFutureImageView.frame.size.width - 20, 40)];
    //uobSummitHeaderLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    skillsFutureHeaderLabel.text = @"SkillsFuture Workshops";
    skillsFutureHeaderLabel.textColor = [UIColor whiteColor];
    skillsFutureHeaderLabel.numberOfLines = 0;
    skillsFutureHeaderLabel.lineBreakMode = NSLineBreakByWordWrapping;
    skillsFutureHeaderLabel.font = [UIFont boldSystemFontOfSize:24];
    [skillsFutureIntroView addSubview:skillsFutureHeaderLabel];
    
    UILabel *skillsFutureTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, skillsFutureHeaderLabel.frame.origin.y + skillsFutureHeaderLabel.frame.size.height + 10, skillsFutureIntroView.frame.size.width - (skillsFutureIntroView.frame.size.width / 3), 20)];
    //uobSummitTitleLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    skillsFutureTitleLabel.text = @"What is SkillsFuture?";
    skillsFutureTitleLabel.textColor = [UIColor whiteColor];
    skillsFutureTitleLabel.numberOfLines = 0;
    skillsFutureTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    skillsFutureTitleLabel.font = [UIFont boldSystemFontOfSize:16];
    [skillsFutureIntroView addSubview:skillsFutureTitleLabel];
    
    UILabel *skillsFutureDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(skillsFutureTitleLabel.frame.origin.x, skillsFutureTitleLabel.frame.origin.y + skillsFutureTitleLabel.frame.size.height, skillsFutureIntroView.frame.size.width  - 20, 60)];
    //uobSummitDescriptionLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    skillsFutureDescriptionLabel.text = @"SkillsFuture is a national movement to enable all Singaporeans to develop to their fullest potential throughout life. Whichever stage of life you are in, whether you.... read more";
    skillsFutureDescriptionLabel.textColor = [UIColor whiteColor];
    skillsFutureDescriptionLabel.font = [UIFont italicSystemFontOfSize:12];
    skillsFutureDescriptionLabel.numberOfLines = 0;
    skillsFutureDescriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [skillsFutureIntroView addSubview:skillsFutureDescriptionLabel];
    
    UIButton *skillsFutureButton = [UIButton buttonWithType:UIButtonTypeSystem];
    skillsFutureButton.frame = CGRectMake(0, _screenHeight / 2, _screenWidth, _screenHeight / 2);
    //uobSummitButton.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:skillsFutureButton];
    
    [skillsFutureButton addTarget:self action:@selector(loadSkillsFuturePage) forControlEvents:UIControlEventTouchUpInside];
}

-(void)initMenuController{
    _mc = [[MenuController alloc] initWithFrame:CGRectMake(-180, 0, 180, _screenHeight)];
    _mc.delegate = self;
    _mc.menu.settings.textColor = [UIColor colorWithRed:5/255.f green:11/255.f blue:103/255.f alpha:1.0];
    [self.view addSubview:_mc];
    
    NSLog(@"list from matrix = %@",_mc.matrixToList);
}

/*
 * Delegates
 */

-(void)didSelectMenu:(MenuController *)menuController row:(NSInteger)row section:(NSInteger)section{
    
    if(self.view.tag == row){
        NSLog(@"I'am clicking the view controller that is already presented");
    }else{
        
        id vc = _mc.menu.viewControllers[row][section];
        
        if([vc isKindOfClass:[NSString class]]){
            NSLog(@"sorry");
        }else{
            
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    NSLog(@"row menu = %ld", (long)row);
}
@end

/*
 CATransition* transition = [CATransition animation];
 transition.duration = 0.5;
 transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
 transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
 transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
 [self.navigationController.view.layer addAnimation:transition forKey:nil];
 */
